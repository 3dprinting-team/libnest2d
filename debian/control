Source: libnest2d
Priority: optional
Maintainer: Debian 3-D Printing Packages <3dprinter-general@lists.alioth.debian.org>
Uploaders:
 Gregor Riepl <onitake@gmail.com>,
 Christoph Berg <myon@debian.org>,
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 libboost-dev,
 libnlopt-cxx-dev,
 libpolyclipping-dev,
Standards-Version: 4.5.0
Section: libs
Homepage: https://github.com/Ultimaker/libnest2d
Vcs-Browser: https://salsa.debian.org/3dprinting-team/libnest2d
Vcs-Git: https://salsa.debian.org/3dprinting-team/libnest2d.git

Package: libnest2d-dev
Section: libdevel
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: 2D irregular bin packaging and nesting C++ header-only library
 Libnest2D is a library and framework for the 2D bin packaging problem.
 Inspired from the SVGNest Javascript library the project is built from scratch
 in C++11. The library is written with a policy that it should be usable out of
 the box with a very simple interface but has to be customizable to the very
 core as well. The algorithms are defined in a header only fashion with
 templated geometry types. These geometries can have custom or already existing
 implementation to avoid copying or having unnecessary dependencies.
 .
 A default backend is provided if the user of the library just wants to use it
 out of the box without additional integration. This backend is reasonably fast
 and robust, being built on top of boost geometry and the polyclipping library.
 Usage of this default backend implies the dependency on these packages but its
 header only as well.
